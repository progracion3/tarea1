var estudiantes = [];

function llenarEstudiante() {
    var estudiante = {
        nombre: document.querySelector('#nombre').value,
        apellido1: document.querySelector('#apellido1').value,
        apellido2: document.querySelector('#apellido2').value,
        curso: document.querySelector('#curso').value
    };
    return estudiante;
}

function Guardar() {
    var estuguard = llenarEstudiante();

    if (estuguard.nombre.trim() !== "" && estuguard.apellido1.trim() !== "" && estuguard.curso.trim() !== "") {
        if (estuguard.apellido2.trim() === "") {
            estuguard.apellido2 = "No tiene";
        }
        estudiantes.push(estuguard);
        mostrarEstudiantes();
        limpiarFormulario();
    } else {
        alert('Por favor, complete todos los campos.');
    }
}

function mostrarEstudiantes() {
    var tablabody = document.querySelector('#tablabody');
    tablabody.innerHTML = '';

    for (var i = 0; i < estudiantes.length; i++) {
        (function(index) {
            var fila = tablabody.insertRow();

            var celdaNombre = fila.insertCell(0);
            celdaNombre.innerHTML = estudiantes[index].nombre;

            var celdaApellido1 = fila.insertCell(1);
            celdaApellido1.innerHTML = estudiantes[index].apellido1;

            var celdaApellido2 = fila.insertCell(2);
            celdaApellido2.innerHTML = estudiantes[index].apellido2;

            var celdaCurso = fila.insertCell(3);
            celdaCurso.innerHTML = estudiantes[index].curso;

            var celdaAcciones = fila.insertCell(4);
            var botonLimpiar = document.createElement('button');
            botonLimpiar.textContent = 'Limpiar';
            botonLimpiar.classList.add('boton-limpiar');
            botonLimpiar.onclick = function() {
                eliminarEstudiante(index);
            };
            celdaAcciones.appendChild(botonLimpiar);
        })(i);
    }
}

function limpiarFormulario() {
    document.querySelector('#nombre').value = '';
    document.querySelector('#apellido1').value = '';
    document.querySelector('#apellido2').value = '';
    document.querySelector('#curso').value = '';
}

function LimpiarFormulario() {
    limpiarFormulario();
}

function eliminarEstudiante(index) {
    estudiantes.splice(index, 1);
    mostrarEstudiantes();
}
